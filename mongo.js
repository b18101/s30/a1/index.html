/*

	Activity: (database: session30)

	//Aggregate to count the total number of items supplied by Red Farms Inc. ($count stage)

	//Aggregate to count the total number of items with price greater than 50. ($count stage)

	//Aggregate to get the average price of all fruits that are onSale per supplier.($group)

	//Aggregate to get the highest price of fruits that are onSale per supplier. ($group)

	//Aggregate to get the lowest price of fruits that are onSale per supplier. ($group)


	//save your query/commands in the mongo.js

*/

db.fruits.aggregate([
	{	
		$match: {supplier: "Red Farms Inc."}
	},
	{
		$count: "itemsRedFarms"
	}
])

db.fruits.aggregate([
	{	
		$match: {price: {$gt: 50}}
	},
	{
		$count: "itemsGreaterThan50"
	}
])

db.fruits.aggregate([
	{
		$match: {onSale: true}	
	},
	{
		$group: {_id: "$supplier", avgPricePerSupplier: {$avg: "$price"}}
	}

])

db.fruits.aggregate([
	{
		$match: {onSale: true}	
	},
	{
		$group: {_id: "$supplier", maxPricePerSupplier: {$max: "$price"}}
	}
])

db.fruits.aggregate([
	{
		$match: {onSale: true}	
	},
	{
		$group: {_id: "$supplier", minPricePerSupplier: {$min: "$price"}}
	}
])